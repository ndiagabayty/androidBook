package com.ngomb.ngomchat;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout mDisplayName;
    private TextInputLayout mEmail;
    private TextInputLayout mMotDePasse;

    private Toolbar toolbar;

    private Button mCreatebtn;
    String displayName;

    //progresse Dialogue

    private ProgressDialog mProgressDiag;

    //firebase authentification

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mProgressDiag = new ProgressDialog(this);

        toolbar = (Toolbar)findViewById(R.id.register_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initanced firbase object

        mAuth = FirebaseAuth.getInstance();

        //recupered fields

        mDisplayName = (TextInputLayout) findViewById(R.id.display_name);
        mEmail = (TextInputLayout) findViewById(R.id.email);
        mMotDePasse = (TextInputLayout)findViewById(R.id.mot_de_passe);

        mCreatebtn = (Button)findViewById(R.id.reg_create_btn);

        mCreatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //recuperation des champs saisiespar l'utilisateur

                String displayName = mDisplayName.getEditText().getText().toString();
                String email = mEmail.getEditText().getText().toString();
                String motDePasse = mMotDePasse.getEditText().getText().toString();

                //affichage de la progression Dialogue

                if(!TextUtils.isEmpty(displayName) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(motDePasse)) {

                    mProgressDiag.setTitle("Register user ");
                    mProgressDiag.setMessage("Please wait while the registration !");

                    //progresse n'interompera pas si l'utilisateur tape sur l'ecran
                    mProgressDiag.setCanceledOnTouchOutside(false);

                    mProgressDiag.show();

                    registerUser(displayName,email,motDePasse);

                }



            }
        });
    }

    private void registerUser(final String displayName, String email, String motDePasse) {

        mAuth.createUserWithEmailAndPassword(email,motDePasse).addOnCompleteListener(this,new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()) {


                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                    String uid = currentUser.getUid();

                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(uid);

                    HashMap<String, String> userMap = new HashMap<>();

                    userMap.put("name",displayName);
                    userMap.put("status","Hi there , I'm using Sen chat App");
                    userMap.put("image","default");
                    userMap.put("thumb_image","default");

                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()) {

                                mProgressDiag.dismiss();

                                Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);

                                finish();

                            }
                        }
                    });

                }else {

                    mProgressDiag.hide();

                    Toast.makeText(getApplicationContext(),"Error in a register ",Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
