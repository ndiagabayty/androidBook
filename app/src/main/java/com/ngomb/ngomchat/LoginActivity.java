package com.ngomb.ngomchat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    //Recuperation des champs

    private TextInputLayout mEmail;

    private TextInputLayout mPassword;

    private Button loginBtn;

    //recuperation de mon toolbar hahaha

    Toolbar toolbarLogin;



    private ProgressDialog mProgressDiag;


    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolbarLogin =(Toolbar) findViewById(R.id.login_toolbar);

        setSupportActionBar(toolbarLogin);
        getSupportActionBar().setTitle("Login page");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //instanciation de notre firebaAuthentification
        mAuth = FirebaseAuth.getInstance();

        //instanciation de notre progresse bar

        mProgressDiag = new ProgressDialog(this);

        //recuperation des champs
        mEmail = (TextInputLayout) findViewById(R.id.login_email);
        mPassword = (TextInputLayout)findViewById(R.id.login_motDePasse);

        loginBtn = (Button)findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            String email = mEmail.getEditText().getText().toString();
            String password = mPassword.getEditText().getText().toString();

            //affichage de la proresse Dialogue

                mProgressDiag.setTitle("Login account");
                mProgressDiag.setMessage("Please wait while we verifie");
                mProgressDiag.setCanceledOnTouchOutside(false);

                //pour afficher la progresse dialogue

                mProgressDiag.show();

               if(!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {

                   loginUser(email,password);

               }else {
                   mProgressDiag.dismiss();
                   Toast.makeText(getApplicationContext(),"Veillez remplir les champs svp!!!",Toast.LENGTH_LONG).show();
               }
            }
        });

    }

    private void loginUser(String email, String password) {

        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()) {

                    mProgressDiag.dismiss();

                Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);

                //Pour renvooyer l'utilisateur vers sa page login si toute il se reconnecte

                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);

                }else {

                    mProgressDiag.hide();

                    Toast.makeText(getApplicationContext(),"Login ou mot de passe incorrect ,or please check your connexion",Toast.LENGTH_LONG).show();
                }

            }
        });
    }


}


