package com.ngomb.ngomchat;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ngomb on 29/11/2017.
 */

class SectionPagerAdapter extends FragmentPagerAdapter {

    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {

            case 0:
                RequestFragment mRequestFragment = new RequestFragment();

                return mRequestFragment;

            case 1:
                ChatsFragment mChatsFragment = new ChatsFragment();

                return mChatsFragment;

            case 2:
                FriendsFragment mFriendsFragment = new FriendsFragment();

                return  mFriendsFragment;

                default:
                    return null;
        }


    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position ) {

        switch(position) {

            case 0:

                return "REQUEST";

            case 1:

                return "CHATS";

            case 2:

                return "FRIENDS";

                default:
                    return null;
        }
    }
}
