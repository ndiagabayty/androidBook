package com.ngomb.ngomchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;


public class UserActivity extends AppCompatActivity {

   private Toolbar mToolbar;
   private RecyclerView musers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

       mToolbar = findViewById(R.id.users_toolbar);
       musers = (RecyclerView) findViewById(R.id.users_list);
    }
}
