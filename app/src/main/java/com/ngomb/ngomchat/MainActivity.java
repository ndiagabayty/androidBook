package com.ngomb.ngomchat;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Toolbar toolbar;

    private TabLayout mTabLayout;

    private ViewPager mViewPager ;

    private SectionPagerAdapter mSectionPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar)findViewById(R.id.main_page_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sen chat");

        //page

        mViewPager = (ViewPager)findViewById(R.id.main_tabPager);

        mSectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionPagerAdapter);

        mTabLayout = (TabLayout)findViewById(R.id.main_tabLayout);

        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly. by the following methode
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){

            sendToStart();
        }

    }

    private void sendToStart() {

        //methode qui renvoie vers la page d'acceuil

        Intent start_intent = new Intent(MainActivity.this , StartActivity.class);

        startActivity(start_intent);

        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.main_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       super.onOptionsItemSelected(item);

       if(item.getItemId() == R.id.main_logout_btn) {

           //L'utilisateur se deconnecte

           FirebaseAuth.getInstance().signOut();

           sendToStart();
       }

       if(item.getItemId() == R.id.main_settings_btn) {

           Intent settingsItent = new Intent(MainActivity.this , SettingsActivity.class );

           TextView dislayName = (TextView)findViewById(R.id.display_name);

           startActivity(settingsItent);
       }
        if(item.getItemId() == R.id.main_users_list) {

            Intent usersIntent = new Intent(MainActivity.this , UsersActivity.class );

            startActivity(usersIntent);
        }

        return true;
    }
}
